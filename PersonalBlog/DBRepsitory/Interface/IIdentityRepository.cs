﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Models;

namespace DBRepsitory.Interface
{
    public interface IIdentityRepository
    {
        Task<User> GetUser(string userName);
    }
}