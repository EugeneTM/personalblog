﻿using System.Threading.Tasks;
using DBRepository.Repositories;
using DBRepsitory.Interface;
using Models;
using Microsoft.EntityFrameworkCore;

namespace DBRepsitory.Repository
{
    public class IdentityRepository : BaseRepository, IIdentityRepository
    {
        public IdentityRepository(string connectionString, IRepositoryContextFactory contextFactory) : base(connectionString, contextFactory) { }

        public async Task<User> GetUser(string userName)
        {
            using (var context = ContextFactory.CreateDbContext(ConnectionString))
            {
                return await context.Users.FirstOrDefaultAsync(u => u.Login == userName);
            }
        }
    }
}